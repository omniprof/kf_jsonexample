/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv659.kf_jsonexample;

import com.cejv659.json_generator.EmployeeJSONGenerator;
import com.cejv659.json_parser.EmployeeJSONParser;
import com.cejv659.json_reader.EmployeeJSONReader;
import com.cejv659.json_writer.EmployeeJSONWriter;
import com.cejv659.model.Employee;
import com.cejv659.persistence.FakeEmployeeDAO;

/**
 *
 * @author Ken
 */
public class JSONDemo {

    private final FakeEmployeeDAO fed;

    public JSONDemo() {
        fed = new FakeEmployeeDAO();
    }

    private void performEmployeeJSONWriter() {
        String fileName = "employee_by_writer.json";
        Employee emp = fed.createEmployee();
        EmployeeJSONWriter ejw = new EmployeeJSONWriter();
        ejw.employeeWriter(emp, fileName);

    }

    private void performEmployeeJSONReader() {
        String fileName = "employee_by_writer.json";
        EmployeeJSONReader ejr = new EmployeeJSONReader();
        ejr.employeeReader(fileName);
    }

    private void performEmployeeJSONParser() {
        String fileName = "employee_by_generator.json";
        EmployeeJSONParser ejp = new EmployeeJSONParser();
        ejp.employeeParser(fileName);

    }

    private void performEmployeeJSONGenerator() {
        Employee emp = fed.createEmployee();
        String fileName = "employee_by_generator.json";
        EmployeeJSONGenerator ejg = new EmployeeJSONGenerator();
        ejg.employeeGenerator(emp, fileName);
    }

    public void perform() {
        // Write
        performEmployeeJSONWriter();
        // Read
        performEmployeeJSONReader();
        //Write
        performEmployeeJSONGenerator();
        // Read
        performEmployeeJSONParser();
    }

    public static void main(String[] args) {
        JSONDemo jd = new JSONDemo();
        jd.perform();
        System.exit(0);
    }

}
