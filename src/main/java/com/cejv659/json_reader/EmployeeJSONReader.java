package com.cejv659.json_reader;

import com.cejv659.model.Address;
import com.cejv659.model.Employee;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import org.slf4j.LoggerFactory;

/**
 * Given a filename, reads the file assuming that it is in JSON format with data
 * for an Employee and enters the data into an Employee object. Derived from
 * http://www.journaldev.com/2315/java-json-example
 *
 * @author Ken Fogel
 *
 */
public class EmployeeJSONReader {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());

    /**
     * Read the file. This is a method that is coupled to the format of the JSON
     * file
     *
     * @param fileName
     * @return
     */
    public Employee employeeReader(String fileName) {

        // The Employee object we plan to return.
        Employee emp = null;
        // Path to the file
        Path emp_file = Paths.get(fileName);

        try ( //create JsonReader object
                InputStream is = Files.newInputStream(emp_file);
                JsonReader jsonReader = Json.createReader(is)) {
            //get JsonObject from JsonReader
            JsonObject jsonObject = jsonReader.readObject();

            //Retrieve data from JsonObject and create Employee bean
            emp = new Employee();
            emp.setEmployeeId(jsonObject.getInt("employeeId"));
            emp.setLastName(jsonObject.getString("lastName"));
            emp.setFirstName(jsonObject.getString("firstName"));
            emp.setPermanentStatus(jsonObject.getBoolean("permanent"));
            emp.setRole(jsonObject.getString("role"));

            //Reading arrays from json
            JsonArray jsonArray = jsonObject.getJsonArray("phoneNumbers");
            long[] numbers = new long[jsonArray.size()];
            int index = 0;
            for (JsonValue value : jsonArray) {
                numbers[index++] = Long.parseLong(value.toString());
            }
            emp.setPhoneNumbers(numbers);

            //reading inner object from json object
            JsonObject innerJsonObject = jsonObject.getJsonObject("address");
            Address address = new Address();
            address.setStreetNumber(innerJsonObject.getInt("streetNumber"));
            address.setStreet(innerJsonObject.getString("street"));
            address.setCity(innerJsonObject.getString("city"));
            address.setProvince(innerJsonObject.getString("province"));
            address.setPostalCode(innerJsonObject.getString("postalCode"));
            emp.setAddress(address);

            //print employee bean information
            log.info("EmployeeJSONReader\n" + emp.toString());

        } catch (IOException ex) {
            log.error("Error reading JSON", ex);
        }
        return emp;
    }

}
