package com.cejv659.persistence;

import com.cejv659.model.Address;
import com.cejv659.model.Employee;

/**
 * This class fakes a data access object and returns a single hard coded
 * Employee object
 *
 * Derived from http://www.journaldev.com/2315/java-json-example
 *
 * @author Ken Fogel
 *
 */
public class FakeEmployeeDAO {

    public Employee createEmployee() {

        Employee emp = new Employee();
        emp.setEmployeeId(100);
        emp.setLastName("David");
        emp.setFirstName("Smith");
        emp.setPermanentStatus(false);
        emp.setPhoneNumbers(new long[]{5145555151L, 6135559191L});
        emp.setRole("Manager");

        Address add = new Address();
        add.setStreetNumber(123);
        add.setStreet("Sesame Street");
        add.setCity("Ottawa");
        add.setProvince("Ontario");
        add.setPostalCode("H0H0H0");
        emp.setAddress(add);
        return emp;
    }
}
