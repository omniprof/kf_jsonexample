package com.cejv659.model;

import java.util.Arrays;

/**
 * Employee class for JSON demo
 *
 * Derived from http://www.journaldev.com/2315/java-json-example
 *
 * @author Ken Fogel
 *
 */
public class Employee {

    private int employeeId;
    private String lastName;
    private String firstName;
    private boolean permanentStatus;
    private Address address;
    private long[] phoneNumbers;
    private String role;

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean isPermanentStatus() {
        return permanentStatus;
    }

    public void setPermanentStatus(boolean permanentStatus) {
        this.permanentStatus = permanentStatus;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public long[] getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(long[] phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("***** Employee Details *****\n");
        sb.append("ID=").append(employeeId).append("\n");
        sb.append("Last Name=").append(lastName).append("\n");
        sb.append("First Name=").append(firstName).append("\n");
        sb.append("Permanent=").append(permanentStatus).append("\n");
        sb.append("Role=").append(role).append("\n");
        sb.append("Phone Numbers=").append(Arrays.toString(phoneNumbers)).append("\n");
        sb.append("Address=").append(getAddress().toString());
        sb.append("\n*****************************");

        return sb.toString();
    }
}
