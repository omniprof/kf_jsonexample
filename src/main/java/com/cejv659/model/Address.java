package com.cejv659.model;

/**
 * Address class for JSON demo
 *
 * Derived from http://www.journaldev.com/2315/java-json-example
 *
 * @author Ken Fogel
 *
 */
public class Address {

    private int streetNumber;
    private String street;
    private String city;
    private String province;
    private String postalCode;

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "Address{" + "streetNumber=" + streetNumber + ", street=" + street + ", city=" + city + ", province=" + province + ", postalCode=" + postalCode + '}';
    }

}
