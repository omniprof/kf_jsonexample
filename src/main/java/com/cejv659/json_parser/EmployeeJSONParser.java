package com.cejv659.json_parser;

import com.cejv659.model.Address;
import com.cejv659.model.Employee;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;
import org.slf4j.LoggerFactory;

public class EmployeeJSONParser {

    /**
     * Given a filename, parses the file similarly to StAX by pulling with
     * events and enters the data into an Employee object. Derived from
     * http://www.journaldev.com/2315/java-json-example
     *
     * @author Ken Fogel
     *
     */
    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());

    public void employeeParser(String fileName) {
        // Path to the file
        Path emp_file = Paths.get(fileName);

        try ( //create JsonReader object
                InputStream is = Files.newInputStream(emp_file);
                JsonParser jsonParser = Json.createParser(is)) {
            Employee emp = new Employee();
            Address address = new Address();
            String keyName = null;
            List<Long> phoneNums = new ArrayList<>();

            // JSON parser events
            while (jsonParser.hasNext()) {
                Event event = jsonParser.next();
                switch (event) {
                    case KEY_NAME:
                        keyName = jsonParser.getString();
                        break;
                    case VALUE_STRING:
                        setStringValues(emp, address, keyName, jsonParser.getString());
                        break;
                    case VALUE_NUMBER:
                        setNumberValues(emp, address, keyName, jsonParser.getLong(), phoneNums);
                        break;
                    case VALUE_FALSE:
                        setBooleanValues(emp, address, keyName, false);
                        break;
                    case VALUE_TRUE:
                        setBooleanValues(emp, address, keyName, true);
                        break;
                    case VALUE_NULL:
                        // don't set anything
                        break;
                    default:
                    // we are not looking for other events
                }
            }

            // Assemble the Employee object
            emp.setAddress(address);
            long[] nums = new long[phoneNums.size()];
            int index = 0;
            for (Long l : phoneNums) {
                nums[index++] = l;
            }
            emp.setPhoneNumbers(nums);

            log.info("EmployeeJSONParser\n" + emp.toString());
        } catch (IOException ex) {
            log.error("Error reading JSON", ex);
        }
    }

    /**
     * Event handler for numeric values. valus arrive as long integers so they
     * must be casted to int where required.
     *
     * @param emp
     * @param address
     * @param keyName
     * @param value
     * @param phoneNums
     */
    private void setNumberValues(Employee emp, Address address,
            String keyName, long value, List<Long> phoneNums) {
        switch (keyName) {
            case "streetNumber":
                address.setStreetNumber((int) value);
                break;
            case "employeeId":
                emp.setEmployeeId((int) value);
                break;
            case "phoneNumbers":
                phoneNums.add(value);
                break;
            default:
                log.info("Unknown element with key=" + keyName);
        }
    }

    /**
     * Event handler for boolean values
     *
     * @param emp
     * @param address
     * @param key
     * @param value
     */
    private void setBooleanValues(Employee emp, Address address,
            String key, boolean value) {
        if ("permanent".equals(key)) {
            emp.setPermanentStatus(value);
        } else {
            log.info("Unknown element with key=" + key);
        }
    }

    /**
     * Event handler for String values
     *
     * @param emp
     * @param address
     * @param key
     * @param value
     */
    private void setStringValues(Employee emp, Address address,
            String key, String value) {
        switch (key) {
            case "firstName":
                emp.setFirstName(value);
                break;
            case "lastName":
                emp.setLastName(value);
                break;
            case "role":
                emp.setRole(value);
                break;
            case "city":
                address.setCity(value);
                break;
            case "province":
                address.setProvince(value);
                break;
            case "street":
                address.setStreet(value);
                break;
            case "postalCode":
                address.setPostalCode(value);
                break;
            default:
                log.info("Unkonwn Key=" + key);
        }
    }
}
