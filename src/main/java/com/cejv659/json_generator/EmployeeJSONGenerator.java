package com.cejv659.json_generator;

import com.cejv659.model.Employee;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import org.slf4j.LoggerFactory;

/**
 * Given an object of type Employee this class writes it to a text file in JSON
 * format using a JSONGenerator. Derived from
 * http://www.journaldev.com/2315/java-json-example
 *
 * @author Ken Fogel
 *
 */
public class EmployeeJSONGenerator {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());

    /**
     * Given an Employee object and a filename write the JSON directly to a
     * stream
     *
     * @param emp
     * @param fileName
     */
    public void employeeGenerator(Employee emp, String fileName) {

        try {
            Path emp_file = Paths.get(fileName);

            // Writes the data as it reads it from the Employee object
            try (OutputStream os = Files.newOutputStream(emp_file);
                    JsonGenerator jsonGenerator = Json.createGenerator(os);) {
                jsonGenerator.writeStartObject(); // start of Employee object
                jsonGenerator.write("employeeId", emp.getEmployeeId());
                jsonGenerator.write("lastName", emp.getLastName());
                jsonGenerator.write("firstName", emp.getFirstName());
                jsonGenerator.write("role", emp.getRole());
                jsonGenerator.write("permanent", emp.isPermanentStatus());

                jsonGenerator.writeStartArray("phoneNumbers"); //start of phone num array
                for (long num : emp.getPhoneNumbers()) {
                    jsonGenerator.write(num);
                }
                jsonGenerator.writeEnd(); // end of phone num array

                jsonGenerator.writeStartObject("address") //start of Address object
                        .write("streetNumber", emp.getAddress().getStreetNumber())
                        .write("street", emp.getAddress().getStreet())
                        .write("city", emp.getAddress().getCity())
                        .write("province", emp.getAddress().getProvince())
                        .write("postalCode", emp.getAddress().getPostalCode())
                        .writeEnd(); //end of Address object

                jsonGenerator.writeEnd(); // end of Employee object
            }
        } catch (IOException ex) {
            log.error("Error writing JSON", ex);
        }
        log.info("EmployeeJSONGenerator finished.");
    }
}
