package com.cejv659.json_writer;

import com.cejv659.model.Employee;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import org.slf4j.LoggerFactory;

/**
 * Given an object of type Employee it creates a JSON Object and then writes it
 * to a file. Derived from http://www.journaldev.com/2315/java-json-example
 *
 * @author Ken Fogel
 *
 */
public class EmployeeJSONWriter {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());

    /**
     * Entry point for this class. Writes the Employee object to a file
     *
     * @param emp
     * @param fileName
     */
    public void employeeWriter(Employee emp, String fileName) {

        JsonObject empJsonObject = buildEmployee(emp);
        writeJSONToFile(empJsonObject, fileName);

    }

    /**
     * Converts the Employee object into a JSON object
     *
     * @param empJsonObject
     */
    private JsonObject buildEmployee(Employee emp) {

        // Create JSON Builders for each component of the Employee object
        JsonObjectBuilder empBuilder = Json.createObjectBuilder();
        JsonObjectBuilder addressBuilder = Json.createObjectBuilder();
        JsonArrayBuilder phoneNumBuilder = Json.createArrayBuilder();

        // Prepare the array of phone numbers
        for (long phone : emp.getPhoneNumbers()) {
            phoneNumBuilder.add(phone);
        }

        // Prepare the Address object
        addressBuilder.add("streetNumber", emp.getAddress().getStreetNumber())
                .add("street", emp.getAddress().getStreet())
                .add("city", emp.getAddress().getCity())
                .add("province", emp.getAddress().getProvince())
                .add("postalCode", emp.getAddress().getPostalCode());

        // Prepare the Employee object
        empBuilder.add("employeeId", emp.getEmployeeId())
                .add("lastName", emp.getLastName())
                .add("firstName", emp.getFirstName())
                .add("role", emp.getRole())
                .add("permanent", emp.isPermanentStatus());

        empBuilder.add("phoneNumbers", phoneNumBuilder);
        empBuilder.add("address", addressBuilder);

        // Retrieve a JSON Object from the builder
        JsonObject empJsonObject = empBuilder.build();

        log.info("EmployeeJSONWriter\n" + empJsonObject);

        return empJsonObject;
    }

    /**
     * Writes the JSON object to a file
     *
     * @param empJsonObject
     */
    private void writeJSONToFile(JsonObject empJsonObject, String fileName) {
        try {
            Path emp_file = Paths.get(fileName);

            // Given an InputStrean a JsonWriter can write to a file
            try (OutputStream os = Files.newOutputStream(emp_file);
                    JsonWriter jsonWriter = Json.createWriter(os)) {
                jsonWriter.writeObject(empJsonObject);
            }
        } catch (IOException ex) {
            log.error("Error writing JSON", ex);
        }
    }

}
